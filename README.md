

# LaserUI

A Python/Qt UI for the [Resolved Instruments](https://resolvedinstruments.com/) x [Phase Sensors](https://www.phasesensors.com/) fouling sensor.

### Installation

LaserUI has been tested on Windows 10 with Python 3.7+.  To install, or upgrade, from a terminal run:
```
py -3 -m pip install --upgrade git+https://gitlab.com/resolvedinstruments/ri-laserui.git@master
```

This will download and install LaserUI and all of it's dependencies.  The command will also install an executable script to the Python 3 scripts directory, which, if in the system path, can be run from a terminal with
```
laserui
```

Or, using the Python Windows launcher, can be run with
```
py -3 -m laserui
```

#### Desktop Shortcut

Instead of running from a terminal, a shortcut can be created to launch LaserUI. To do this, right-click and add a new shortcut to the desktop.  When prompted for location, enter `py -3 -m laserui`, and then for the name, enter "LaserUI".

Double-clicking the shortcut will now launch LaserUI.