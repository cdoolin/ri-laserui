import QtQuick 2.10
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.3

Pane {
    width: 400
    property int margin: 5
    height: gridLayout.implicitHeight + 2 * margin

    //    height: 125
    property alias label: title.text
    property alias enableSwitch: enableSwitch
    property alias dtInput: dtInput

    property bool enabled: true
    property bool small: false

    ColumnLayout {
        id: gridLayout
        anchors.fill: parent
        spacing: 0
        anchors.margins: margin

        RowLayout {
            Layout.preferredWidth: parent.width
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            Text {
                id: title
                text: "Downsample data"
                font.pixelSize: small ? 16 : 20
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true
            }

            Switch {
                id: enableSwitch
                Layout.alignment: Qt.AlignRight
                enabled: true
            }
        }

        GridLayout {
            //            Layout.fillWidth: true
            Layout.preferredWidth: parent.width
            Layout.topMargin: 10
            columns: 3

            Text {
                text: "Sample spacing:"
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
                enabled: enabled
            }

            TextField {
                id: dtInput
                //                Layout.fillWidth: true
                text: "10"
                horizontalAlignment: Text.AlignRight
                Layout.preferredWidth: 100
                font.pixelSize: small ? 12 : 18
                selectByMouse: true
            }

            Text {
                text: "s"
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
            }
        }

        // Ramp/Target Grid
    }
}
