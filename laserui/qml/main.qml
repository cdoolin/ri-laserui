import QtQuick 2.12
// import QtQuick.Window 2.10
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.11

import LaserUI 1.0


ApplicationWindow {
    id: window
    visible: true
    width: 1600
    height: 900
    title: qsTr("LaserUI")

    FileFinder {
        id: finder
    }

    FontLoader {
        id: awesomesolid
        // source: "qrc:/qml/fa-solid-900.ttf"
        source: finder.find("fa-solid-900.ttf")
//        source: "fa-solid-900.ttf"
//        source: "qrc:/qml/fa-regular-400.ttf"
    }

    FaVars {
        id: fa
    }

    LaserController {
        id: theController
    }

    Heater {
        id: theHeater
    }

    SystemPalette { 
        id: activePallete; 
        colorGroup: SystemPalette.Active 
    }
    SystemPalette { 
        id: disabledPallete; 
        colorGroup: SystemPalette.Disabled 
    }

    DataLogger {
        id: theLogger
        rate: 10
        running: false

        controller: theController
        heater: theHeater
    }


    header: ToolBar {
        RowLayout {
            anchors.fill: parent

            
            Label {
                Layout.fillWidth: true
            }
            
            ToolButton {
                contentItem: FaIcon {
                    text: fa.fa_sliders_h
                    color: enabled ? activePallete.text : disabledPallete.text
                }

                onClicked: panelBox.state = 'connect';
            }

            // ToolButton {
            //     contentItem: FaIcon {
            //         text: fa.fa_sliders_h
            //         color: enabled ? activePallete.text : disabledPallete.text
            //     }

            //     onClicked: panelBox.state = 'control';
            // }
            ToolButton {
                contentItem: FaIcon {
                    text: fa.fa_chart_line
                    color: enabled ? activePallete.text : disabledPallete.text
                }

                onClicked: panelBox.state = 'plot';
            }

            Label {
                Layout.fillWidth: true
            }

        }
    }

    
    property int npanels: 2
    property int pwidth: window.width
    property int pheight: panelBox.height


    Item {
        id: panelBox

        width: pwidth * npanels
//        height: window.height
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        state: "connect"



        ConnectPanel {
            id: connectPanel
            width: pwidth
            height: pheight
            x: pwidth * 0
        }

        // ControlPanel {
        //     id: controlPanel
        //     width: pwidth
        //     height: pheight
        //     x: pwidth * 1

        // }

        PlotPanel {
            id: plotPanel
            width: pwidth
            height: pheight
            x: pwidth * 1

            enabled: panelBox.state === "plot"
        }

        states: [
            State {
                name: "connect"
                PropertyChanges {
                    target: panelBox
                    x: -pwidth * 0
                }
            },

            // State {
            //     name: "control"
            //     PropertyChanges {
            //         target: panelBox
            //         x: -pwidth * 1
            //     }
            // },

            State {
                name: "plot"
                PropertyChanges {
                    target: panelBox
                    x: -pwidth * 1
                }
            }

        ]
        transitions:
            Transition {
            to: "*"
            NumberAnimation {
                target: panelBox;
                properties: "x"
                duration: 200
                easing.type: Easing.OutQuad
            }
        }
    }




}
