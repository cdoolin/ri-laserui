import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13

Pane {
    width: theLayout.implicitWidth
    height: theLayout.implicitHeight

    property bool isRunning: false

    property alias playButton: playButton
    property alias stopButton: stopButton
    property alias loadButton: loadButton
    property alias saveButton: saveButton

    RowLayout {
        id: theLayout
        anchors.fill: parent

        Button {
            id: playButton
            visible: !isRunning

            Layout.preferredWidth: 50
            Layout.preferredHeight: 50
            Layout.alignment: Qt.AlignCenter

            contentItem: FaIcon {
                text: fa.fa_play
                color: enabled ? activePallete.text : disabledPallete.text
            }
        }

        Button {
            id: stopButton
            Layout.preferredWidth: 50
            Layout.preferredHeight: 50
            Layout.alignment: Qt.AlignCenter

            contentItem: FaIcon {
                text: fa.fa_stop
                color: enabled ? activePallete.text : disabledPallete.text
            }

            visible: isRunning
        }

        Button {
            id: saveButton
            Layout.preferredWidth: 50
            Layout.preferredHeight: 50
            Layout.alignment: Qt.AlignCenter

            contentItem: FaIcon {
                text: fa.fa_save
                color: enabled ? activePallete.text : disabledPallete.text
            }

            enabled: !isRunning
        }

        Button {
            id: loadButton
            Layout.preferredWidth: 50
            Layout.preferredHeight: 50
            Layout.alignment: Qt.AlignCenter

            contentItem: FaIcon {
                text: fa.fa_folder_open
                color: enabled ? activePallete.text : disabledPallete.text
            }

            enabled: !isRunning
        }
    }
}
