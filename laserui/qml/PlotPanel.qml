import QtQuick 2.10
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.13

import LaserUI 1.0



Item {
    id: plotPanel
    property alias enabled: laserplot.enabled

    RowLayout {
        anchors.fill: parent

        LaserPlot {
            id: laserplot
            Layout.fillHeight: true
            Layout.fillWidth: true

            logger: theLogger

            dpi: 120
        }

        
        ColumnLayout {
            PlotControls {
                logger: theLogger
                // Layout.alignment: Qt.AlignRight
            }

            ResamplerControls {
                logger: theLogger
                small: true
                Layout.preferredWidth: 350
            }

            CurrentController {
                id: laser1550
                label: "Signal Laser"
                small: true

                laser: theController.laser2

                // Layout.fillWidth: true
                Layout.preferredWidth: 350
            }



            HeatingLaserController {
                label: "Heating Laser"
                small: true

                laser: theController.laser1
                Layout.preferredWidth: 350
            }

            HeaterController {
                label: "Heater"
                small: true
                heater: theHeater

                // Layout.fillWidth: true
                Layout.preferredWidth: 350

            }


            Item {
                Layout.fillHeight: true
            }

        }

    }

}
