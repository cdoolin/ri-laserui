import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13
import QtGraphicalEffects 1.0


Item {
    width: 600

    GridLayout {
        anchors.fill: parent
        anchors.margins: 15
        columnSpacing: 15
        rowSpacing: 15

        columns: 2
        layoutDirection: GridLayout.LeftToRight


        CurrentController {
            id: laser1550
            label: "1550 Current"

            laser: theController.laser2

            Layout.fillWidth: true
            Layout.maximumWidth: 300
            Layout.alignment: Qt.AlignRight

            layer.enabled: true
            layer.effect: DropShadow {
                horizontalOffset: 2
                verticalOffset: 2
                transparentBorder: true
                color: "#606060"
                radius: 5
            }
        }



        CurrentController {
            label: "980 Current"
            
            laser: theController.laser1
            Layout.fillWidth: true
            Layout.maximumWidth: 300
            Layout.alignment: Qt.AlignLeft

            layer.enabled: true
            layer.effect: DropShadow {
                horizontalOffset: 2
                verticalOffset: 2
                transparentBorder: true
                color: "#606060"
                radius: 5
            }
        }

        TECController {
            label: "1550 Temp."

            tec: theController.tec2

            Layout.fillWidth: true
            Layout.maximumWidth: 300
            Layout.alignment: Qt.AlignRight

            layer.enabled: true
            layer.effect: DropShadow {
                horizontalOffset: 2
                verticalOffset: 2
                transparentBorder: true
                color: "#606060"
                radius: 5
            }
        }

        TECController {
            label: "980 Temp."

            tec: theController.tec1

            Layout.fillWidth: true
            Layout.maximumWidth: 300
            Layout.alignment: Qt.AlignLeft

            layer.enabled: true
            layer.effect: DropShadow {
                horizontalOffset: 2
                verticalOffset: 2
                transparentBorder: true
                color: "#606060"
                radius: 5
            }
        }

        PDController {
            label: "Photodiode"

            Layout.fillWidth: true
            Layout.maximumWidth: 300
            Layout.alignment: Qt.AlignRight | Qt.AlignTop


            layer.enabled: true
            layer.effect: DropShadow {
                horizontalOffset: 2
                verticalOffset: 2
                transparentBorder: true
                color: "#606060"
                radius: 5
            }
        }



        HeaterController {
            label: "Heater"
            heater: theHeater

            Layout.fillWidth: true
            Layout.maximumWidth: 300
            Layout.alignment: Qt.AlignLeft

            layer.enabled: true
            layer.effect: DropShadow {
                horizontalOffset: 2
                verticalOffset: 2
                transparentBorder: true
                color: "#606060"
                radius: 5
            }
        }

//        Item {
//            Layout.fillWidth: true
//        }


        Item {
            Layout.fillHeight: true
        }
        Item {
            Layout.fillHeight: true
        }
    }



}


