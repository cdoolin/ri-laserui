import QtQuick 2.10
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.3

Pane {
    width: 400
    property int margin: 5
    height: gridLayout.implicitHeight + 2 * margin

    //    height: 125
    property alias label: title.text
    property alias onSwitch: onSwitch
    property alias slider: slider
    property alias currentInput: currentInput
    property alias maxText: maxText

    property bool small: false
    property bool connected: false

    GridLayout {
        id: gridLayout
        anchors.fill: parent

        //        anchors.left: parent.left
        //        anchors.right: parent.right
        anchors.margins: margin
        columns: 3
        flow: GridLayout.LeftToRight
        rowSpacing: 0
        columnSpacing: 0

        Item {
            Layout.preferredWidth: parent.width / 3
        }
        Item {
            Layout.preferredWidth: parent.width / 3
        }
        Item {
            Layout.preferredWidth: parent.width / 3
        }

        Row {
            spacing: 10
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            Text {
                id: title
                text: "laser 1"
                font.pixelSize: small ? 16 : 20
            }

            FaIcon {
                text: fa.fa_exclamation_circle
                color: "#ba4401"
                visible: !connected
                font.pixelSize: small ? 16 : 20
            }
        }

        Switch {
            id: onSwitch
            Layout.alignment: Qt.AlignRight
            visible: !small
        }

        Slider {
            id: slider

            //            width: parent.width
            Layout.columnSpan: 3
            Layout.fillWidth: true
        }

        Text {
            text: "0 mA"
            font.pixelSize: small ? 12 : 14

            //            Layout.preferredWidth: parent.width / 3
            Layout.alignment: Qt.AlignLeft
        }

        Row {
            TextInput {
                id: currentInput
                text: "10"
                font.pixelSize: small ? 12 : 18
                selectByMouse: true
            }

            Text {
                text: " mA"
                font.pixelSize: small ? 12 : 18
            }

            Layout.alignment: Qt.AlignCenter
        }

        Row {
            Text {
                id: maxText
                text: "40"
                font.pixelSize: small ? 12 : 14
            }
            Text {
                text: " mA"
                font.pixelSize: small ? 12 : 14
            }

            Layout.alignment: Qt.AlignRight
        }
    }
}
