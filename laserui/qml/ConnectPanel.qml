import QtQuick 2.10
import QtQuick.Layouts 1.12

import QtGraphicalEffects 1.0

import LaserUI 1.0



Item {
    id: connectPanel
    // property alias enabled: laserplot.enabled
    property bool enabled: false

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 20

        GridLayout {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            columns: 2

            ConnectorThingForm {
                label: "Laser Controller"
                connected: theController.connected

                refreshButton.onClicked: theController.update_ports()
                connectButton.onClicked: theController.connectPort(portCombo.currentIndex)
                disconnectButton.onClicked: theController.disconnect()

                portCombo.model: theController.ports
                portCombo.currentIndex: theController.suggestedPort


                Layout.alignment: Qt.AlignHCenter
                Layout.columnSpan: 2
                Layout.fillWidth: true
            }



            CurrentController {
                id: laser1550
                label: "Signal Laser"

                laser: theController.laser2

                Layout.fillWidth: true
                Layout.preferredWidth: 400
                Layout.maximumWidth: 500
                Layout.alignment: Qt.AlignRight

                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 2
                    verticalOffset: 2
                    transparentBorder: true
                    color: "#606060"
                    radius: 5
                }
            }



            HeatingLaserController {
                label: "Heating Laser"

                laser: theController.laser1
                Layout.fillWidth: true
                Layout.preferredWidth: 400
                Layout.maximumWidth: 500
                Layout.alignment: Qt.AlignLeft

                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 2
                    verticalOffset: 2
                    transparentBorder: true
                    color: "#606060"
                    radius: 5
                }
            }

            TECController {
                label: "Signal Laser Temp."

                tec: theController.tec2

                Layout.fillWidth: true
                Layout.preferredWidth: 400
                Layout.maximumWidth: 500
                Layout.alignment: Qt.AlignRight

                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 2
                    verticalOffset: 2
                    transparentBorder: true
                    color: "#606060"
                    radius: 5
                }
            }

            TECController {
                label: "Heating Laser Temp."

                tec: theController.tec1

                Layout.fillWidth: true
                Layout.preferredWidth: 400
                Layout.maximumWidth: 500
                Layout.alignment: Qt.AlignLeft

                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 2
                    verticalOffset: 2
                    transparentBorder: true
                    color: "#606060"
                    radius: 5
                }
            }

            PDController {
                label: "Signal"

                Layout.fillWidth: true
                Layout.preferredWidth: 400
                Layout.maximumWidth: 500
                Layout.alignment: Qt.AlignRight | Qt.AlignTop


                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 2
                    verticalOffset: 2
                    transparentBorder: true
                    color: "#606060"
                    radius: 5
                }
            }

        } // GridLayout

        GridLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            columns: 1


            ConnectorThingForm {
                label:  "Heater"
                connected: theHeater.connected

                refreshButton.onClicked: theHeater.update_ports()
                connectButton.onClicked: theHeater.connectPort(portCombo.currentIndex)
                disconnectButton.onClicked: theHeater.disconnect()

                portCombo.model: theHeater.ports
                portCombo.currentIndex: theHeater.suggestedPort

                Layout.preferredWidth: 500
//                Layout.columnSpan: 2
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true
            }


            HeaterController {
                label: "Heater"
                heater: theHeater

                Layout.fillWidth: true
                Layout.preferredWidth: 400
                Layout.maximumWidth: 500
                Layout.alignment: Qt.AlignHCenter

                layer.enabled: true
                layer.effect: DropShadow {
                    horizontalOffset: 2
                    verticalOffset: 2
                    transparentBorder: true
                    color: "#606060"
                    radius: 5
                }
            }

        } // GridLayout
    } // Row


}
