import QtQuick 2.4

HeaterControllerForm {
    label: "heater"

    QtObject {
        id: nullHeater
        property real temp: 0
        property real setpoint: 30
        property bool enabled: true
        property bool connected: false
    }
    property var heater: nullHeater
    connected: heater.connected

    setpointInput.text: heater.setpoint.toFixed(3)
    setpointInput.onAccepted: {
        heater.setpoint = parseFloat(setpointInput.text);
    }

    tempText.text: heater.temp.toFixed(3)

//    onSwitch.checked: heater.enabled
//    onSwitch.enabled: heater.connected
//    onSwitch.onToggled: {
//        heater.enabled = onSwitch.checked;
//    }


}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
