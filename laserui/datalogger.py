

import numpy as np


import qtpy
# from qtpy import QtGui, QtWidgets, QtCore, QtQml
from qtpy.QtCore import QObject, Signal, Slot, Property
from qtpy.QtCore import qDebug
from qtpy.QtCore import QStringListModel, QTimer, QUrl

import time
import os


class DataLogger(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._timer = QTimer(self)
        self._rate = 10 # Hz

        self._controller = None
        self._laser = None
        self._heater = None
        self._last_t = time.perf_counter()

        self._timer.timeout.connect(self.update)


        # for downsampling of sample data
        self._downsample_dt = 1.
        self._downsample_enabled = True
        self._downsampled_to = 0 
        self.max_vals = 1e6

        self.vals_t = []
        self.vals_pd = []
        self.vals_curr = []
        self.vals_heat = []

        # self.

    forceUpdate = Signal()

    def _get_rate(self):
        return self._rate

    def _set_rate(self, rate):
        if rate != self._rate:
            self._rate = rate

            if self.running:
                self.stop()
                self.start()

            self.rateChanged.emit()
    
    rateChanged = Signal()
    rate = Property(float, _get_rate, _set_rate, notify=rateChanged)


    def _get_running(self):
        return self._timer.isActive()

    def _set_running(self, run):
        if run and not self.running:
            self.start()
            self.runningChanged.emit()
        if not run and self.running:
            self.stop()
            self.runningChanged.emit()

    runningChanged = Signal()
    running = Property(bool, _get_running, _set_running, notify=runningChanged)

    @Slot()
    def stop(self):
        self._timer.stop()
        self.runningChanged.emit()


    @Slot()
    def start(self):
        self.reset()

        self._timer.start(1000. / self._rate)
        self.runningChanged.emit()


    @Slot()
    def reset(self):
        self.vals_t = []
        self.vals_pd = []
        self.vals_curr = []
        self.vals_heat = []
        self._downsampled_to = 0

    def _get_controller(self):
        return self._controller

    def _set_controller(self, c):
        if self._controller is not c:
            self._controller = c
            if c is not None:
                self._laser = c.laser1
            else:
                self._laser = None

            self.controllerChanged.emit()

    controllerChanged = Signal()
    controller = Property(QObject, _get_controller, _set_controller, notify=controllerChanged)
    
    def _get_heater(self):
        return self._heater

    def _set_heater(self, h):
        if self._heater is not h:
            self._heater = h
            self.heaterChanged.emit()

    heaterChanged = Signal()
    heater = Property(QObject, _get_heater, _set_heater, notify=heaterChanged)



    @Slot()
    def update(self):
        """
        called when _timer goes off and causes new samples to be read.
        """
        t = time.perf_counter()
        dt = t - self._last_t
        self._last_t = t
        # qDebug("%.3f ms" % (dt * 1e3))

        if self._controller is not None:
            pd = self._controller.read_pd() 
            curr = self._laser.read_current()
        else:
            pd = 0
            curr = 0

        if self._heater is not None:
            h = self._heater.read_temp()
        else:
            h = 0

        self.vals_t.append(t)
        self.vals_pd.append(pd)
        self.vals_curr.append(curr)
        self.vals_heat.append(h)

        self.update_downsampling()

    def update_downsampling(self):
        if not self._downsample_enabled:
            return

        if len(self.vals_t) - self._downsampled_to < 2. * self._rate:
            return

        t0 = self.vals_t[self._downsampled_to]
        t1 = self.vals_t[-1]
        dt = self._downsample_dt

        # resample to constant sample rate
        # res_dt = 1. / self._rate
        # res_t = np.arange(t0, t1, res_dt)

        # res_pd = np.interp(res_t, self.vals_t[self._downsampled_to:], self.vals_pd[self._downsampled_to:])
        # res_curr = np.interp(res_t, self.vals_t[self._downsampled_to:], self.vals_curr[self._downsampled_to:])
        # res_heat = np.interp(res_t, self.vals_t[self._downsampled_to:], self.vals_heat[self._downsampled_to:])

        # downsample with simple interpolation
        # todo:  resample to constant samplerate and filter before downsampling
        new_t = np.arange(t0, t1, dt)
        new_pd = np.interp(new_t, self.vals_t[self._downsampled_to:], self.vals_pd[self._downsampled_to:])
        new_curr = np.interp(new_t, self.vals_t[self._downsampled_to:], self.vals_curr[self._downsampled_to:])
        new_heat = np.interp(new_t, self.vals_t[self._downsampled_to:], self.vals_heat[self._downsampled_to:])

        last_i = next(i for i, t in enumerate(self.vals_t[self._downsampled_to:])
                if t > new_t[-1]) + self._downsampled_to
        # last_i = len(self.vals_t) - last_i
        # qDebug(f"alsti: {last_i}")

        oldn = len(self.vals_t)

        # qDebug(f"dtto: {self.vals_t[self._downsampled_to]}  lastt: {self.vals_t[last_i]}")
        # qDebug(f"oldt: {self.vals_t}")

        self.vals_t = self.vals_t[:self._downsampled_to] + list(new_t) + self.vals_t[last_i:]
        self.vals_pd = self.vals_pd[:self._downsampled_to] + list(new_pd) + self.vals_pd[last_i:]
        self.vals_curr = self.vals_curr[:self._downsampled_to] + list(new_curr) + self.vals_curr[last_i:]
        self.vals_heat = self.vals_heat[:self._downsampled_to] + list(new_heat) + self.vals_heat[last_i:]
        
        self._downsampled_to = self._downsampled_to + len(new_t) - 1


        # qDebug(f"newt: {self.vals_t}")
        # qDebug(f"downsampled {oldn} to {len(self.vals_t)}. at {self._downsampled_to}.")



    @Slot(QUrl)
    def save(self, url):
        path = url.toLocalFile()

        npdata = np.vstack((
            np.asarray(self.vals_t),
            np.asarray(self.vals_pd),
            np.asarray(self.vals_curr),
            np.asarray(self.vals_heat),
        ))

        _, ext = os.path.splitext(path)

        if ext == ".npy":
            np.save(path, npdata)
        elif ext == ".csv":
            np.savetxt(path, npdata.T, delimiter=',', header="Time (s), Photocurrent (%), Heating Laser (mA), Heater Temp (C)")

        qDebug(f"save: {path}, {str(npdata.shape)}")

        
        
    @Slot(QUrl)
    def load(self, url):
        path = url.toLocalFile()

        _, ext = os.path.splitext(path)

        if ext == ".npy":
            npdata = np.load(path)
        elif ext == ".csv":
            npdata = np.loadtxt(path, delimiter=',', skiprows=1).T


        self.vals_t = list(npdata[0])
        self.vals_pd = list(npdata[1])
        self.vals_curr = list(npdata[2])
        self.vals_heat = list(npdata[3])

        self._downsampled_to = len(self.vals_t)
        self.downsampleEnabled = False

        qDebug(f"load: {path}, {npdata.shape}")
        self.forceUpdate.emit()

    def _get_downsampleEnabled(self):
        return self._downsample_enabled

    def _set_downsampleEnabled(self, e):
        if self._downsample_enabled is not e:
            self._downsample_enabled = e

            if e:
                # force resampling of data when toggled
                self._downsampled_to = 0

                if not self.running:
                    self.update_downsampling()

            self.downsampleEnabledChanged.emit()

    downsampleEnabledChanged = Signal()
    downsampleEnabled = Property(bool, _get_downsampleEnabled, _set_downsampleEnabled, notify=downsampleEnabledChanged)




    def _get_downsampleDt(self):
        return self._downsample_dt

    def _set_downsampleDt(self, dt):
        if self._downsample_dt is not dt:
            self._downsample_dt = dt
            self.downsampleDtChanged.emit()

    downsampleDtChanged = Signal()
    downsampleDt = Property(float, _get_downsampleDt, _set_downsampleDt, notify=downsampleEnabledChanged)





from .qmatplotlib import QMPLFigure


class LaserPlot(QMPLFigure):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.plot_init()

        self._logger = None
        self._enabled = False

        self._updatetimer = QTimer(self)
        self._updatetimer.timeout.connect(self.updateplot)
        self._updatetimer.start(500)

        self._i = 0

    def get_logger(self):
        return self._logger
    
    def set_logger(self, c):
        if c is not self._logger:
            if self._logger is not None:
                c.forceUpdate.disconnect(self.update)
            c.forceUpdate.connect(self.update)
            self._logger = c
            self.loggerChanged.emit()

    loggerChanged = Signal()
    logger = Property(QObject, get_logger, set_logger, notify=loggerChanged)



    def plot_init(self):
        fig = self.figure
        # fig.set_tight_layout(True)
        ax1, ax2, ax3 = self.axs = fig.subplots(3, 1, sharex=True)
        fig.subplots_adjust(
            left=0.07, right=0.98, 
            bottom=0.07, top=0.98, 
            hspace=0)
        ax1.tick_params(direction='in')
        ax2.tick_params(direction='in')
        ax3.tick_params(direction='in')
        

    def plot_draw(self):
        l = self._logger
        ax1, ax2, ax3 = self.axs


        if len(l.vals_t) < 1:
            return

            
        downsample = int(len(l.vals_t) / 5000. + 1.)

        ts = np.asarray(l.vals_t[::downsample])
        pd = np.asarray(l.vals_pd[::downsample])
        curr = np.asarray(l.vals_curr[::downsample])
        heat = np.asarray(l.vals_heat[::downsample])

        ts = ts - ts[0]

        if ts[-1] > 1.5*3600:
            ts = ts / 3600.
            tunits = "hours"
        elif ts[-1] > 120:
            ts = ts / 60.
            tunits = "min"
        else:
            tunits = "s"

        lengths = [len(ts), len(pd), len(curr), len(heat)]
        if not all(i == lengths[0] for i in lengths):
            print(f"lists not same length: {str(lengths)}")
            return

        ax1.clear()
        ax1.plot(ts, pd, '-')
        ax1.set_ylabel("Photocurrent (%)")

        ax2.clear()
        ax2.plot(ts, curr, '-')
        ax2.set_ylabel("Heating Laser (mA)")

        ax3.clear()
        ax3.plot(ts, heat, '-')
        ax3.set_ylabel(u"Heater Temp (\u00b0C)")
        ax3.set_xlabel("Time ({0})".format(tunits))
        ax3.set_xlim(ts[0], ts[-1])
        

    @Slot()
    def updateplot(self):
        if self._enabled is not True or self._logger is None:
            return

        if not self._is_drawing:
            self.canvas.draw_func = self.plot_draw
            self.draw_threaded()
        else:
            pass
            # qDebug("is drawing skip")

    def get_enabled(self):
        return self._enabled

    def set_enabled(self, en):
        en = bool(en)
        if self._enabled != en:
            self._enabled = en

            if self._enabled:
                self.updateplot()

            self.enabled_changed.emit()

    enabled_changed = Signal()
    enabled = Property(bool, get_enabled, set_enabled, notify=enabled_changed)
