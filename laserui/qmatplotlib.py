

import qtpy
# from qtpy import QtCore
from qtpy.QtCore import QObject, Signal, Slot, Property, Qt
from qtpy.QtCore import qDebug
from qtpy.QtQuick import QQuickPaintedItem, QQuickWindow
from qtpy.QtGui import QColor, QImage
# from qtpy.QtCore import QStringListModel, QTimer

from qtpy.QtCore import QThread, QMutex


# Matplotlib has Qt5 backend, but uses Widgets which isn't compatable in QtQuick application
# (Can have QtQuick in a widgets application)

# https://matplotlib.org/faq/howto_faq.html#matplotlib-in-a-web-application-server

from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg

from matplotlib import cbook
from matplotlib.transforms import Bbox


class DrawerWorker(QObject):
    def __init__(self, canvas, parent=None):
        super().__init__(parent)
        self.canvas = canvas
        self.mutex = canvas.mutex
        self.figure = canvas.figure


    done = Signal()

    @Slot()
    def work(self):
        self.mutex.lock()
        if not self.canvas.ok():
            self.figure.set_size_inches(
                self.canvas.isize_win,
                self.canvas.isize_hin,
                forward=False)
            self.canvas.set_csize(self.canvas.isize_win, self.canvas.isize_hin)

        if self.canvas.draw_func is not None:
            self.canvas.draw_func()
            self.canvas.draw_func = None

        self.canvas.draw()
        self.mutex.unlock()

        self.done.emit()

class ThreadedFigureCanvasAgg(FigureCanvasAgg):
    def __init__(self, figure):
        super().__init__(figure)

        self.mutex = QMutex()

        # csize is current size of canvas
        self.set_csize(0, 0)

        # isize is size of QQuickItem.  ie. size canvas should be
        # and will be changed to next time a draw is performed
        self.set_isize(1, 1)

        self.draw_func = None


    def set_isize(self, win, hin):
        self.isize_win = win
        self.isize_hin = hin

    def set_csize(self, win, hin):
        self.csize_win = win
        self.csize_hin = hin

    def ok(self):
        return (self.csize_win == self.isize_win and
                self.csize_hin == self.isize_hin)

class QMPLFigure(QQuickPaintedItem):
    # https://doc.qt.io/qtforpython/PySide2/QtQuick/QQuickPaintedItem.html#PySide2.QtQuick.QQuickPaintedItem


    def __init__(self, parent=None):
        super().__init__(parent=parent)

        # self.setAcceptedMouseButtons(Qt.AllButtons)
        self.figure = Figure()
        self.canvas = ThreadedFigureCanvasAgg(self.figure)
        self.mutex = self.canvas.mutex

        self._thread = QThread()
        self._thread.start()

        self._worker = DrawerWorker(self.canvas)
        
        self._do_draw_threaded.connect(self._worker.work)
        self._worker.done.connect(self._draw_threaded_done)

        self._worker.moveToThread(self._thread)

        self._is_drawing = False


        

        self.widthChanged.connect(self.resize_canvas)
        self.heightChanged.connect(self.resize_canvas)
        self.windowChanged.connect(self.resize_canvas)


    # @Property(float)
    @property
    def _dpi_ratio(self):
        if self.window() is not None:
            return self.window().effectiveDevicePixelRatio()
        else:
            return 1

    
    def get_dpi(self):
        return self.figure.dpi
    
    def set_dpi(self, dpi):
        if self.figure.dpi != dpi:
            self.figure.set_dpi(float(dpi))
            self.resize_canvas()
            self.dpi_changed.emit()

    dpi_changed = Signal()
    dpi = Property(float, get_dpi, set_dpi, notify=dpi_changed)

    _do_resize = Signal(float, float)


    @Slot()
    def resize_canvas(self):
        dpi_ratio = self._dpi_ratio
        w = self.width() * dpi_ratio
        h = self.height() * dpi_ratio

        dpival = self.figure.dpi
        winch = w / dpival
        hinch = h / dpival
            
        if winch > 0 and hinch > 0:
            self.canvas.set_isize(winch, hinch)
            self.draw_threaded()


    _do_draw_threaded = Signal()

    def draw_threaded(self):
        if not self._is_drawing:
            self._is_drawing = True
            self._do_draw_threaded.emit()

    @Slot()
    def _draw_threaded_done(self):
        self._is_drawing = False

        if self.canvas.ok():
            self.update()
        else:
            self.draw_threaded()
            

    def mousePressEvent(self, event):
        qDebug(f"mouse press!")
        event.ignore()


    def paint(self, painter):
        # If the canvas does not have a renderer, then give up and wait for
        # FigureCanvasAgg.draw(self) to be called.
        if not hasattr(self.canvas, 'renderer'):
            qDebug("canvas does not have renderer")
            return 


        if self.canvas.ok() and self.canvas.mutex.tryLock():
            cw, ch = self.canvas.get_width_height()
            w = self.width()
            h = self.height()

            if cw != w or ch != h:
                qDebug(f" w: {w}  h: {h}")
                qDebug(f"cw: {cw} ch: {ch}")

            buf = self.canvas.buffer_rgba()
            qimage = QImage(buf, cw, ch, QImage.Format_RGBA8888)
            painter.drawImage(0, 0, qimage)

            self.canvas.mutex.unlock()
        # else:
            # canvas in use, try again later.sss
            # pass
            # self.update()
        




