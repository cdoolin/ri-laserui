


import qtpy
# from qtpy import QtGui, QtWidgets, QtCore, QtQml
from qtpy.QtCore import QObject, Signal, Slot, Property
from qtpy.QtCore import qDebug
from qtpy.QtCore import QStringListModel, QTimer


import minimalmodbus
# import serial
import serial.tools.list_ports



class QHeater(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._i = None
        self._temp = None
        self._setpoint = 20
        
        self._ports = []
        self._qports = QStringListModel(["thing"])
        self.update_ports()

    # property connected: bool 
    def _get_connected(self):
        # qDebug(f"connected: {str(self._i)}")
        return self._i is not None

    connectedChanged = Signal()
    connected = Property(bool, _get_connected, notify=connectedChanged)


    def read(self, register, default=0):
        if not self.connected:
            return default
        
        tries = 0
        r = None
        while r is None and tries < 3:
            try:
                r = self._i.read_long(register)
            except minimalmodbus.NoResponseError:
                qDebug("error reading ({0})".format(register))
                tries += 1
                r = None

        if r is None:
            self.disconnect()
            r = default

        return r

    def write(self, register, val):
        if not self.connected:
            return
        
        try:
            self._i.write_long(register, int(val))
        except minimalmodbus.NoResponseError:
            self.disconnect()



        

    @Slot(str)
    def connect(self, port):
        try:
            self._i = minimalmodbus.Instrument(port, 1)
            self._i.serial.baudrate = 9600   # Baud
            self._i.serial.bytesize = 8
            self._i.serial.parity   = serial.PARITY_NONE
            self._i.serial.stopbits = 1
            self._i.serial.timeout  = 0.5   # seconds
            self._i.mode = minimalmodbus.MODE_RTU   # rtu or ascii mode
            qDebug("opened port \"%s\"" % port)
            self.on_connected()

        except serial.SerialException:
            qDebug("unable to open port '%s'" % port)
            self._i = None
        
        self.connectedChanged.emit()

    @Slot(int)
    def connectPort(self, porti):
        port = self._ports[porti].device
        self.connect(port)

    def on_connected(self):
        self.read_temp()
        self._get_setpoint(update=True)


    @Slot()
    def disconnect(self):
        """ Disconnects heater
        """
        if self._i is not None:
            # self._i.close()
            del self._i
            self._i = None
            self.connectedChanged.emit()




    def read_temp(self):
        self._temp = self.read(20, default=0.) / 1000.
        if self.connected:
            self.tempChanged.emit()
        return self._temp

    def _get_temp(self):
        if self._temp is None:
            self.read_temp()

        return self._temp
    
    tempChanged = Signal()
    temp = Property(float, _get_temp, notify=tempChanged)
    


    def _get_setpoint(self, update=False):
        if update:
            self._setpoint = self.read(27) / 1000.
            qDebug(f"sp: {self._setpoint}")
            self.setpointChanged.emit()

        return self._setpoint

    def _set_setpoint(self, setpoint):
        if setpoint != self._setpoint:
            self._setpoint = setpoint
            self.setpointChanged.emit()

        self.write(27, self._setpoint * 1000.)
    

    setpointChanged = Signal()
    setpoint = Property(float, _get_setpoint, _set_setpoint, notify=setpointChanged)
    
    def _get_enabled(self):
        return True

    def _set_enabled(self, en):
        pass

    enabledChanged = Signal()
    enabled = Property(bool, _get_enabled, _set_enabled, notify=enabledChanged)

    



    @Slot()
    def update_ports(self):
        self._ports = serial.tools.list_ports.comports()
        pnames = [str(p.description) for p in self._ports]
        if len(pnames) is 0:
            pnames.append("No Ports")
        self._qports.setStringList(pnames)
        self.suggestedPortChanged.emit()

    def _get_suggested_port(self):
        def goodp(p):
            return (p.vid == 0x0403 and p.pid == 0x6001)
        
        goodports = [goodp(p) for p in self._ports]

        try:
            p = goodports.index(True)
        except ValueError:
            p = 0
        return p

    suggestedPortChanged = Signal()
    suggestedPort = Property(int, _get_suggested_port, notify=suggestedPortChanged)


    # property ports: QStringListModel
    def get_ports(self):
        return self._qports

    portsChanged = Signal()
    ports = Property(QStringListModel, get_ports, notify=portsChanged)




